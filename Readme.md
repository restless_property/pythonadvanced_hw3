## **The '.csv'-files' processor**
    
        This program consists of a class designed to handle a '.csv'-file
    to calculate the average of mass and height of the measured subjects,
    output the relevant data and write it down into another '.csv'-file.