"""
The program operates a '.csv' file,
computes required measurements
and writes them down into another '.csv'-file.
"""

import csv


class CSVProcessor:
    """
    This class opens and reads a ".csv"-file,
    calculates average altitude, mass and the number
    of subjects, outputs relevant data and writes it down
    into another '.csv'-file.

    Methods:
    read_scv: opens a '.csv'-file
              and makes a 'csv.DictReader' of it
    measure: calculates the average of height, weight
             among the subjects and their quantity
    get_data: outputs necessary data on the calculations
    make_csv: creates a new '.csv'-file of the computed values
    """

    def __init__(self, path: str):
        """
        The 'constructor' method of the class.

        Parameters:
        :param path (str): the path to the '.csv'-file

        Instance/Class attributes:
        self.file (str): an empty string to be replaced with a .csv'-file
        self.CM_AN_INCH,
        self.KG_A_POUND (int): constants with metric ratio
                               of centimeters to inches
                               and kilograms to pounds
        (self.average_cm,
         self.average_kg,
         self.quantity) (int): values of the calculated measurements
         self.content (list): an empty list to be replaced
                              with an operable content of a 'csv'-file
        """
        self.path = path
        self.CM_AN_INCH = 2.54
        self.KG_A_POUND = 0.453592
        self.average_cm, self.average_kg, self.quantity = 0, 0, 0
        self.content = []

    def read_scv(self):
        """
        This method is used to open a '.csv' file,
        convert its content into a csvDictReader'-object.

        :return self.csv_vocabulary (obj): a 'csvDictReader'-object
                                           with '.csv' values
                                           packed into mapping objects
        Local variables:
        file (TextIOWrapper): a wrapped 'Python' representation
                              of a '.csv'-file
        """
        with open(self.path) as file:
            self.content = list(csv.DictReader(file))
        return self.content

    def measure(self):
        """
        This method calculates
        the average of height and weight
        among the subjects
        and sums up their quantity.

        :return:
        (self.average_cm,
         self.average_kg,
         self.quantity) (int): relevant values
                               for the calculated measurements

        Local variables:
        altitude, mass (int): values of personal and
                              total height and weight
                              of the measured subjects
        line (OrderedDict): a collection of data contained
                            within one raw of the '.csv'-file
        """
        if not self.content:
            print("The '.csv'-file has not been read. Cannot initiate the calculation.")
        else:
            altitude, mass = 0, 0
            for line in self.content:
                altitude += float(line['Height(Inches)'])
                mass += float(line['Weight(Pounds)'])
                self.quantity += 1
            try:
                self.average_cm = altitude / self.quantity * self.CM_AN_INCH
                self.average_kg = mass / self.quantity * self.KG_A_POUND
            except ZeroDivisionError:
                print("Division error: quantity=0 !")
        return self.average_cm, self.average_kg, self.quantity

    def get_data(self):
        """
        This GETTER method outputs and returns relevant information
        on the average altitude, mass and the quantity of the subjects.
        :return (self.average_cm,
                self.average_kg,
                self.quantity) (int): average weight, height
                                      and the number of subjects
        """
        print(f"Average height among {self.quantity} participant(s) equals {self.average_cm} cm.")
        print(f"Average weight among {self.quantity} participant(s) equals {self.average_kg} kg.")
        return self.average_cm, self.average_kg, self.quantity

    def make_csv(self):
        """
        This method makes a new '.csv'-file
        of the computed average height, weight
        an of the quantity of the subjects.

        :return average_csv ('TextIOWrapper'): a 'TextWrapper'
                                               with a '.scv'-file
        Local variables:
        fields (list): a list of column names for the new '.csv'-file
        new_csv (csvDictWriter): an object with '.csv' values
                                 packed into 'mapping' objects
        """
        stats = {"Quantity_of_Subjects": self.quantity,
                 "Average_Height(cm)": self.average_cm,
                 "Average_Weight(kg)": self.average_kg}
        with open('../csv/average.csv', 'w') as average_csv:
            print(type(average_csv))
            fields = ['Quantity_of_Subjects', 'Average_Height(cm)', 'Average_Weight(kg)']
            new_csv = csv.DictWriter(average_csv, delimiter='|', fieldnames=fields)
            new_csv.writeheader()
            new_csv.writerow(stats)
        return average_csv


# The point of entry.
# Точка входа в программу.
if __name__ == '__main__':
    # (Creation of an instance of the class 'CSVProcessor'
    #  and application of required methods thereon).
    # (Создание экземпляра класса 'CSVProcessor'
    #  и вызов необходимых методов соответственно).
    print()
    data = CSVProcessor('../csv/data.csv')
    data.read_scv()
    data.measure()
    data.get_data()
    data.make_csv()
